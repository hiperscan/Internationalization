import sys, polib

if len(sys.argv) != 2:
    print 'Usage: %s POFILE' % sys.argv[0]
    sys.exit(0)

po = polib.pofile(sys.argv[1])

for entry in po:
    entry.msgstr = ''

po.save()
