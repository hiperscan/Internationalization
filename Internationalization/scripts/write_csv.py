import sys, polib, codecs

if len(sys.argv) != 2:
    print 'Usage: %s POFILE' % sys.argv[0]
    sys.exit(0)

po = polib.pofile(sys.argv[1])

for entry in po:
    print entry.msgid.encode('utf-8').replace('\n', '; ') + '$' + entry.msgstr.encode('utf-8').replace('\n', '; ') + '$' + entry.comment.encode('utf-8').replace('\n', '; ')

