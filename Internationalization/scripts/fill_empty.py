import sys, polib

if len(sys.argv) != 3:
    print 'Usage: %s SOURCE DESTINATION' % sys.argv[0]
    sys.exit(0)

src_po = polib.pofile(sys.argv[1])
dst_po = polib.pofile(sys.argv[2])

for dst_entry in dst_po:
    if dst_entry.msgstr != '':
	continue
    for src_entry in src_po:
        if dst_entry.msgid == src_entry.msgid:
            dst_entry.msgstr = src_entry.msgstr

dst_po.save()
